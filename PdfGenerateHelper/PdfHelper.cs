﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PdfSharp;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf.IO;
using System.IO;

namespace PdfGenerateHelper
{
    /// <summary>
    /// 
    /// </summary>
    public class PdfHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public PdfDocument CreatePdfFromText(string text)
        {
            PdfDocument pdfDoc = new PdfDocument();
            pdfDoc.Info.Title = "Document";
            PdfPage page = pdfDoc.AddPage();
            XGraphics gfx = XGraphics.FromPdfPage(page);
            XFont font = new XFont("Calibri", 10, XFontStyle.Regular);
            XTextFormatter xf = new XTextFormatter(gfx);
            page.Size = PageSize.A4;
            XRect rect = page.MediaBox.ToXRect();

            // decrease width & height for margin
            rect.Inflate(-20, -20);

            xf.DrawString(text, font, XBrushes.Black, rect, XStringFormats.TopLeft);

            return pdfDoc;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public PdfDocument CreatePdfFromFile(Stream stream, string fileName)
        {
            PdfDocument pdfDoc = new PdfDocument();
            pdfDoc.Info.Title = fileName;
            PdfPage page = pdfDoc.AddPage();
            XGraphics gfx = XGraphics.FromPdfPage(page);
            XFont font = new XFont("Calibri", 10, XFontStyle.Regular);
            XTextFormatter xf = new XTextFormatter(gfx);
            page.Size = PageSize.A4;
            XRect rect = page.MediaBox.ToXRect();

            // decrease width & height for margin
            rect.Inflate(-20, -20);

            // TODO : allow text free flow
            // TODO : multiple pages

            StreamReader reader = new StreamReader(stream);
            string text = reader.ReadToEnd();

            xf.DrawString(text, font, XBrushes.Black, rect, XStringFormats.TopLeft);

            return pdfDoc;
        }

        public PdfDocument CreatePdfFromTemplate()
        {
            return null;
        }
    }
}
