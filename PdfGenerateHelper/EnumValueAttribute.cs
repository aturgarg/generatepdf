﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PdfGenerateHelper
{
    [System.AttributeUsage(AttributeTargets.Field)]
    public class EnumValueAttribute : System.Attribute
    {
        public string DisplayValue = null;        

        public EnumValueAttribute()
        {
        }
    }
}
