﻿namespace GeneratePDF
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGeneratePdfDoc = new System.Windows.Forms.Button();
            this.cbGenerationType = new System.Windows.Forms.ComboBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnChooseFile = new System.Windows.Forms.Button();
            this.lblSelectedFile = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnGeneratePdfDoc
            // 
            this.btnGeneratePdfDoc.Location = new System.Drawing.Point(529, 534);
            this.btnGeneratePdfDoc.Name = "btnGeneratePdfDoc";
            this.btnGeneratePdfDoc.Size = new System.Drawing.Size(104, 23);
            this.btnGeneratePdfDoc.TabIndex = 0;
            this.btnGeneratePdfDoc.Text = "Generate pdf";
            this.btnGeneratePdfDoc.UseVisualStyleBackColor = true;
            this.btnGeneratePdfDoc.Click += new System.EventHandler(this.btnGeneratePdfDoc_Click);
            // 
            // cbGenerationType
            // 
            this.cbGenerationType.FormattingEnabled = true;
            this.cbGenerationType.Location = new System.Drawing.Point(24, 22);
            this.cbGenerationType.Name = "cbGenerationType";
            this.cbGenerationType.Size = new System.Drawing.Size(176, 21);
            this.cbGenerationType.TabIndex = 1;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(650, 534);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(24, 105);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(701, 408);
            this.textBox1.TabIndex = 3;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileToConvertDialog";
            // 
            // btnChooseFile
            // 
            this.btnChooseFile.Location = new System.Drawing.Point(24, 59);
            this.btnChooseFile.Name = "btnChooseFile";
            this.btnChooseFile.Size = new System.Drawing.Size(155, 23);
            this.btnChooseFile.TabIndex = 4;
            this.btnChooseFile.Text = "Browse file";
            this.btnChooseFile.UseVisualStyleBackColor = true;
            this.btnChooseFile.Click += new System.EventHandler(this.btnChooseFile_Click);
            // 
            // lblSelectedFile
            // 
            this.lblSelectedFile.AutoSize = true;
            this.lblSelectedFile.Location = new System.Drawing.Point(199, 69);
            this.lblSelectedFile.MaximumSize = new System.Drawing.Size(400, 0);
            this.lblSelectedFile.MinimumSize = new System.Drawing.Size(100, 0);
            this.lblSelectedFile.Name = "lblSelectedFile";
            this.lblSelectedFile.Size = new System.Drawing.Size(100, 13);
            this.lblSelectedFile.TabIndex = 5;
            this.lblSelectedFile.Text = "Select File";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 569);
            this.Controls.Add(this.lblSelectedFile);
            this.Controls.Add(this.btnChooseFile);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.cbGenerationType);
            this.Controls.Add(this.btnGeneratePdfDoc);
            this.Name = "FormMain";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGeneratePdfDoc;
        private System.Windows.Forms.ComboBox cbGenerationType;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button btnChooseFile;
        private System.Windows.Forms.Label lblSelectedFile;
    }
}

