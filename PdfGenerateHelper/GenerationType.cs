﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PdfGenerateHelper
{
    /// <summary>
    /// 
    /// </summary>
    public enum PdfGenerationType
    {
        /// <summary>
        /// 
        /// </summary>
        [EnumValueAttribute(DisplayValue = "Generate from text")]
        GenerateFromText,

        /// <summary>
        /// 
        /// </summary>
        [EnumValueAttribute(DisplayValue = "Generate from file")]
        GenerateFromFile,

        /// <summary>
        /// 
        /// </summary>
        [EnumValueAttribute(DisplayValue = "Generate from template")]
        GenerateFromTemplate
    }
}
