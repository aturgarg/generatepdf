﻿using PdfGenerateHelper;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeneratePDF
{
    public partial class FormMain : Form
    {
        #region Variables & Properties

        PdfHelper pdfHelper = null;

        #endregion Variables & Properties

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public FormMain()
        {
            InitializeComponent();

            cbGenerationType.DataSource = new BindingSource(ConversionUtility.GetEnumValuesByAttributeValue(typeof(PdfGenerationType)), null);
            cbGenerationType.ValueMember = "Key";
            cbGenerationType.DisplayMember = "Value";
        }

        #endregion Constructor

        #region Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGeneratePdfDoc_Click(object sender, EventArgs e)
        {
            try
            {
                pdfHelper = new PdfHelper();

                var pdfGenerationType = (PdfGenerationType)Enum.Parse(typeof(PdfGenerationType), cbGenerationType.SelectedValue.ToString());
                PdfDocument pdfDoc = null;
                switch (pdfGenerationType)
                {
                    case PdfGenerationType.GenerateFromText:
                        pdfDoc = pdfHelper.CreatePdfFromText(SampleTextConstant.SAMPLE_TEXT);
                        break;
                    case PdfGenerationType.GenerateFromFile:
                        Stream stream = null;
                        if ((stream = openFileDialog.OpenFile()) != null)
                        {
                            pdfHelper = new PdfHelper();
                            pdfDoc = pdfHelper.CreatePdfFromFile(stream, GeneralUtility.ParseFileName(openFileDialog.FileName));
                        }
                        break;
                    case PdfGenerationType.GenerateFromTemplate:
                        break;
                    default:
                        break;
                }

                if (pdfDoc != null)
                {
                    SaveGeneratedPdfFile(pdfDoc);
                }
                else
                {
                    // error message
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChooseFile_Click(object sender, EventArgs e)
        {
            try
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    if (string.IsNullOrWhiteSpace(openFileDialog.FileName))
                    {
                        // show error
                    }
                    else
                    {
                        lblSelectedFile.Text = openFileDialog.FileName;
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                Application.Exit();
            }
            catch (Exception ex)
            {
            }
        }

        #endregion Events

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pdfDoc"></param>
        private static void SaveGeneratedPdfFile(PdfDocument pdfDoc)
        {
            Stream myStream;
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Filter = "pdf files (*.pdf)|*.pdf";
            saveFileDialog.FilterIndex = 1;
            saveFileDialog.RestoreDirectory = true;

            // set a default file name for dialog
            saveFileDialog.FileName = pdfDoc.Info.Title;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveFileDialog.OpenFile()) != null)
                {
                    pdfDoc.Info.Title = saveFileDialog.FileName;
                    pdfDoc.Save(myStream);
                    myStream.Close();
                }
            }
        }

        #endregion Methods
    }
}
