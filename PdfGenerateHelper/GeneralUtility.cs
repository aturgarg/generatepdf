﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PdfGenerateHelper
{
    /// <summary>
    /// 
    /// </summary>
    public class GeneralUtility
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullFilePath"></param>
        /// <returns></returns>
        public static string ParseFileName(string fullFilePath)
        {
            string fileName = string.Empty;
            if (fullFilePath.Contains('\\'))
            {
                fileName = fullFilePath.Split('\\').Last<string>();
                if(fileName.Contains('.'))
                {
                    fileName = fileName.Substring(0, fileName.LastIndexOf('.'));
                }
            }
            return fileName;
        }
    }
}
