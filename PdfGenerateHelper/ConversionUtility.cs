﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PdfGenerateHelper
{
    /// <summary>
    /// 
    /// </summary>
    public class ConversionUtility
    {
        private static Type _EnumAttrType = typeof(EnumValueAttribute);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enumType"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetEnumValuesByAttributeValue(Type enumType)
        {
            var enumValuesCollection = new Dictionary<string, string>();
            IEnumerable enumValues = Enum.GetValues(enumType);

            foreach (var enumVal in enumValues)
            {
                var valueString = enumVal.ToString();

                MemberInfo memInfo = enumType.GetMember(valueString).FirstOrDefault();
                if (null != memInfo)
                {
                    EnumValueAttribute enumAttr = (EnumValueAttribute)(memInfo.GetCustomAttributes(_EnumAttrType, false).FirstOrDefault());
                    if (null != enumAttr)
                    {
                        enumValuesCollection.Add(enumVal.ToString(), enumAttr.DisplayValue);
                    }
                }
            }

            return enumValuesCollection;
        }
    }
}
